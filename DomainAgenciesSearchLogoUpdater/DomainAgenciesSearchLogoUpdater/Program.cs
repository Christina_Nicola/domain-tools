﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using System.Linq;
//using System.Data.Linq;
//using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web;

namespace DomainAgenciesSearchLogoUpdater
{
    class Program
    {
        //environment settings
        static private string _ImagesPath = null;
        static private string _ConnectionString = null;

        static void LogMessage(string message)
        {
            Console.WriteLine("\n" + DateTime.Now.ToShortTimeString() + " - " + message);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("****** Agencies Search Logos Updater Tool ******");
            //choice to select which environment to run
            if (!SelectEnvironment())
                return;

            bool processOnlyAgenciesWithNoSearchLogoInDB = SelectAgenciesProcessingMode();

            bool reindexListings = SelectReindexingMode();

            //confirm settings
            Console.WriteLine("\nTo start processing with the above settings press enter or close to exit");
            Console.ReadLine();

            LogMessage("Starting processing..");
            List<int> agenciesIds = GetAgenciesIds(processOnlyAgenciesWithNoSearchLogoInDB);

            if (agenciesIds != null && agenciesIds.Count > 0)
            {
                LogMessage("Agencies count = " + agenciesIds.Count.ToString());
                
                int agenciesCount = 0;
                List<int> updatedAgenciesIDs = new List<int>();
                foreach (int agencyID in agenciesIds)
                {
                    if (UpdateSearchLogoForAgencyIfLogoExists(agencyID, reindexListings))
                        updatedAgenciesIDs.Add(agencyID);

                    //for debugging
                    agenciesCount++;
                    System.Diagnostics.Debug.Write(agenciesCount.ToString() + "\n");
                }

                LogMessage(string.Format("{0} agencies updated/reindexed from {1} agencies processed.", updatedAgenciesIDs.Count.ToString(), agenciesCount.ToString()));
                
                LogMessage("Processing complete!");

                //if (reindexListings)
                //    ReindexListingsForAgencies(updatedAgenciesIDs);

                //end
                Console.ReadLine();
            }
            else
            {
                LogMessage("No agencies found!");
                Console.ReadLine();
            }
        }

        private static bool SelectReindexingMode()
        {
            //Choose whether to reindex listings
            bool reindexListings = false;

            Console.WriteLine("\nReindex listings for updated agencies?(Y/N)");
            string userEntry = Console.ReadLine();

            switch (userEntry.ToUpper())
            {
                case "Y":
                    reindexListings = true;
                    Console.WriteLine("Listings belonging to updated agencies will be reindexed!");
                    break;
                default:
                    Console.WriteLine("No listings will be reindexed!");
                    break;
            }

            return reindexListings;
        }

        private static bool SelectAgenciesProcessingMode()
        {
            //Choose to process agencies that have no search logo in db or all
            bool processOnlyAgenciesWithNoSearchLogoInDB = false;

            Console.WriteLine("\nRun tool ONLY for agencies with no search logos in the database?(Y/N)");
            string userEntry = Console.ReadLine();
            
            switch (userEntry.ToUpper())
            {
                case "Y":
                    processOnlyAgenciesWithNoSearchLogoInDB = true;
                    Console.WriteLine("Only agencies with no search logos in the database will be processed!");
                    break;
                default:
                    Console.WriteLine("All agencies will be processed!");
                    break;
            }

            return processOnlyAgenciesWithNoSearchLogoInDB;
        }

        private static bool SelectEnvironment()
        {
            Console.WriteLine("\nPlease select the environment (P:Production - S:Staging - D:Develop)");
            string userEntry = Console.ReadLine();

            switch (userEntry.ToUpper())
            {
                case "P"://production
                    _ImagesPath = "http://images.domain.com.au/img/Agencys";
                    _ConnectionString = "Server=APINFPDBV71\\DOM;Database=Domain;User=DomainUser;Password=Poi123Lkj6tf;Pooling=True;Application Name=Domain.ConsumerWebsite;";
                    break;
                case "S"://staging
                    _ImagesPath = "http://uat2.images.domain.com.au/img/Agencys";
                    _ConnectionString = "Server=APDOMSDB031;Database=Domain;UID=DomainUser;Password=9KlGJKZ6;Pooling=True;Application Name=uat2.domain.com.au;";
                    break;
                case "D"://dev
                    _ImagesPath = "http://dev.images.domain.com.au/img/Agencys";
                    _ConnectionString = "Server=PDDOMDBSYD01;Database=Domain;User=sa;Password=bgt76okn49dfg;Pooling=True;Application Name=Domain.ConsumerWebsite;";
                    break;
            }

            if (_ImagesPath != null && _ConnectionString != null)
            {
                Console.WriteLine("\nConnection String: " + _ConnectionString);
                Console.WriteLine("\nImagesPath: " + _ImagesPath);

                Console.WriteLine("\nTo continue with the above settings, please enter Y, or press Enter to exit.");
                userEntry = Console.ReadLine();
                if (userEntry.ToUpper() != "Y")
                {
                    Console.WriteLine("\nClosing application...");
                    return false;
                }
            }
            else
            {
                //ask again for the selection
                return SelectEnvironment();
            }
            return true;
        }

        private static string FormulateCommaSeparatedString(List<int> list)
        {
            StringBuilder stringList = new StringBuilder();
            foreach (int listitem in list)
            {
                stringList.Append(listitem.ToString());
                stringList.Append(',');
            }
            return stringList.ToString().TrimEnd(',');
        }

        private static void ReindexListingsForAgencies(List<int> agencyIDs)
        {
            string agenciesString = FormulateCommaSeparatedString(agencyIDs);

            LogMessage(string.Format("Reindexing listings for agency ID(s) {0} ...",  agenciesString));

            using (SqlConnection connection = new SqlConnection(_ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = string.Format("update Listing set ModifiedDate = GETDATE() where AgencyID in ({0})", agenciesString);

                    command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    LogMessage(e.Message);
                }

                connection.Close();
            }

            LogMessage(string.Format("Reindexing listings complete for agency ID(s) {0} ...", agenciesString));
        }

        private static bool UpdateSearchLogoForAgencyIfLogoExists(int agencyID, bool reindexListings)
        {
            bool agencyUpdated = false;

            if (SearchLogoExists(agencyID))
            {
                UpdateSearchLogoForAgency(agencyID);
                agencyUpdated = true;

                if(reindexListings)
                    ReindexListingsForAgencies(new List<int> {agencyID});
            }

            return agencyUpdated;
        }

        private static void UpdateSearchLogoForAgency(int agencyID)
        {
            string searchLogoName = "searchlogo_" + agencyID.ToString() + ".GIF";

            LogMessage("Updating " + searchLogoName);

            using (SqlConnection connection = new SqlConnection(_ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = string.Format("update Agency set URL_SearchLogo = '{0}' where AgencyID = {1}", searchLogoName, agencyID.ToString());

                    command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    LogMessage(e.Message);
                }

                connection.Close();
            }
        }

        private static bool SearchLogoExists(int agencyID)
        {
            string uri = string.Format("{0}/{1}/searchlogo_{1}.GIF", _ImagesPath, agencyID.ToString());
            
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                request.KeepAlive = false;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                bool result = response.StatusCode == HttpStatusCode.OK; 
                response.Dispose();
                return result;
            }
            catch (Exception)
            {
                //Any exception will return false.

                LogMessage(string.Format("Search Logo not found for agency {0}", agencyID.ToString()));
                return false;
            }
        }

        //we could exclude CRE agencies since they wont use that data anyway 
        private static List<int> GetAgenciesIds(bool onlyAgenciesWithNoSearchLogo)
        {
            LogMessage("Getting agencies ...");
            
            List<int> agenciesIds = null;

            using (SqlConnection connection = new SqlConnection(_ConnectionString))
            {
                SqlDataReader dataReader = null;
                
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = string.Format("select AgencyID from Agency {0}", onlyAgenciesWithNoSearchLogo ? "where URL_SearchLogo is null" : "");//where (Agency NOT like '/_%' ESCAPE '/')

                    dataReader = command.ExecuteReader();
                    if (dataReader.HasRows)
                    {
                        agenciesIds = new List<int>();

                        while (dataReader.Read())
                        {
                            agenciesIds.Add(dataReader.GetInt32(dataReader.GetOrdinal("AgencyID")));
                        }
                    }
                }
                catch (Exception e)
                {
                    LogMessage(e.Message);
                }

                if(dataReader!=null)
                    dataReader.Close();
                
                connection.Close();
            }
            
            return agenciesIds;
        }
    }
}
